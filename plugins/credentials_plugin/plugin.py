from plugin_framework.extension import Extension
from plugins.credentials_plugin.widgets.login_widget import LoginWidget
from PySide2 import QtWidgets

class Plugin(Extension):
    def __init__(self, specification, iface):
        """
        :param iface: main_window aplikacije
        """
        super().__init__(specification, iface)
        self.widget : LoginWidget = LoginWidget(iface)
        self.open_login = QtWidgets.QAction("&Login")
        self.open_register = QtWidgets.QAction("&Register")
        self.open_login.triggered.connect(self.show_login)
        self.open_register.triggered.connect(self.show_register)
        self.deactivate()

    def activate(self):
        self.activated = True
        self.iface.add_menu_action("&Credentials", self.open_login)
        self.iface.add_menu_action("&Credentials", self.open_register)
        print("Activated Credentials Plugin")

    def deactivate(self):
        self.activated = False
        self.iface.remove_menu_action("&Credentials", self.open_login)
        self.iface.remove_menu_action("&Credentials", self.open_register)
        print("Deactivated Credentials Plugin")

    def show_login(self):
        self.widget.show()

    def show_register(self):
        from .widgets.registration_widget import RegistrationWidget
        _widget = RegistrationWidget(self.iface)
        _widget.show()