import json
import os

from PySide2.QtCore import QRect
from PySide2.QtWidgets import *

class RegistrationWidget(QDialog):
    widget_for = 5
    def __init__(self, parent):
        super().__init__(parent)

        self.setWindowTitle("Please enter your registration information...")
        self.resize(337, 102)

        self.pushButton = QPushButton(self)
        self.pushButton.setObjectName(u"pushButton")
        self.pushButton.setGeometry(QRect(130, 70, 75, 23))
        self.widget = QWidget(self)
        self.widget.setObjectName(u"widget")
        self.widget.setGeometry(QRect(40, 10, 261, 51))
        self.formLayout = QFormLayout(self.widget)
        self.formLayout.setObjectName(u"formLayout")
        self.formLayout.setHorizontalSpacing(40)
        self.formLayout.setContentsMargins(5, 0, 0, 0)
        self.label = QLabel(self.widget)
        self.label.setObjectName(u"label")

        self.formLayout.setWidget(0, QFormLayout.LabelRole, self.label)

        self.label_2 = QLabel(self.widget)
        self.label_2.setObjectName(u"label_2")

        self.formLayout.setWidget(1, QFormLayout.LabelRole, self.label_2)

        self.password_field = QLineEdit(self.widget)
        self.password_field.setObjectName(u"lineEdit_2")
        self.password_field.setEchoMode(QLineEdit.Password)

        self.formLayout.setWidget(1, QFormLayout.FieldRole, self.password_field)

        self.username_field = QLineEdit(self.widget)
        self.username_field.setObjectName(u"lineEdit")

        self.formLayout.setWidget(0, QFormLayout.FieldRole, self.username_field)

        self.pushButton.setText("Register")

        self.pushButton.clicked.connect(self.try_register)

        self.label.setText("Username")
        self.label_2.setText("Password")
        self.password_field.setInputMask("")

    def try_register(self):

        accounts_path = "./storage/accounts.json"
        if not os.path.exists(accounts_path):
            return

        with open(accounts_path) as fp:
            data : dict[str] = json.load(fp)

        username = self.username_field.text()
        password = self.password_field.text()
        if data.get(username) is not None:
            msg_box = QMessageBox(self)
            msg_box.setText("Username already taken.")
            msg_box.setWindowTitle("Whoops")
            msg_box.show()
            return

        data[username] = password

        accounts_file = open(accounts_path, "w")
        json.dump(data, accounts_file, indent=6)

        msg_box = QMessageBox(self)
        msg_box.setText("Registered successfully.")
        msg_box.setWindowTitle("Success")
        msg_box.show()
        self.close()