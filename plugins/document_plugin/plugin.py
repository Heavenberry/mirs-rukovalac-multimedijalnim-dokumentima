from plugin_framework.extension import Extension
from .widget import DocumentViewer

class Plugin(Extension):
    def __init__(self, specification, iface):
        """
        :param iface: main_window aplikacije
        """
        super().__init__(specification, iface)
        self.widget : DocumentViewer = DocumentViewer(iface.actions_dict, iface.central_widget, iface.centralWidgetGridLayout)
        self.deactivate()

    def activate(self):
        self.activated = True
        self.widget.setHidden(False)
        print("Activated Document Viewer")

    def deactivate(self):
        self.activated = False
        self.widget.setHidden(True)
        print("Deactivated Document Viewer")