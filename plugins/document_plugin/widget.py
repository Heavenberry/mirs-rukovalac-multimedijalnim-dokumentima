from math import floor

from PySide2 import QtWidgets
from PySide2.QtWidgets import *

class DocumentViewer(QtWidgets.QTabWidget):
    widget_for = 2
    slots : dict[int, list[QWidget]] = dict()
    def __init__(self, actions_dict : dict, parent, layout : QGridLayout):
        super().__init__(parent)
        central_widget_layout = layout
        self._layout = QGridLayout()
        self.setTabsClosable(True)

        tool_bar = QtWidgets.QToolBar("document_toolbar", self)

        tool_bar.addAction(actions_dict["new_slot"])
        tool_bar.addAction(actions_dict["delete_slot"])
        tool_bar.addAction(actions_dict["pop_slot"])

        actions_dict["new_slot"].triggered.connect(self.create_new_slot)
        actions_dict["delete_slot"].triggered.connect(self.delete_selected_slot)
        actions_dict["pop_slot"].triggered.connect(self.pop_slot)
        self.tabBar().currentChanged.connect(self.switched_tab)
        self.tabBar().tabCloseRequested.connect(self.deleteTab)


        central_widget_layout.setMenuBar(tool_bar)

        self.setObjectName(u"documentViewerTabWidget")
        central_widget_layout.addWidget(self, 0, 0, 1, 1)

    def deleteTab(self, index):

        self.remove_all_slots_from_tab_index(index)
        self.removeTab(index)

    def remove_all_slots_from_tab_index(self, index):
        if self.slots.get(index) is None:
            return

        for slot in self.slots.get(index):
            slot.close()

        self.slots.get(index).clear()

        del self.slots[index]

    def switched_tab(self):
        self.regenerate_slot_widgets_layout()

    def get_current_tab_slots_count(self):
        if self.count() <= 0:
            return 0

        current_tab_slots = self.slots.get(self.currentIndex())
        slots_count = 0
        if current_tab_slots is not None:
            slots_count = len(current_tab_slots)

        return slots_count

    def create_new_slot(self):
        if self.count() <= 0:
            return

        slots_count = self.get_current_tab_slots_count()

        if slots_count <= 0:
            self._layout = QGridLayout(self.currentWidget())

        new_slot = QTextEdit(self)
        new_slot.setVisible(True)

        row_index = slots_count % 4
        if row_index < 0:
            row_index = 0
        column_index = floor(slots_count / 4)
        if column_index < 0:
            column_index = 0

        self._layout.addWidget(new_slot, row_index, column_index, 1, 1)

        slot_list : list[QWidget] = list()
        if self.slots.get(self.currentIndex()) is not None:
            slot_list = list(self.slots[self.currentIndex()])

        slot_list.append(new_slot)
        self.slots[self.currentIndex()] = slot_list

    def delete_selected_slot(self):
        if self.count() <= 0:
            return

        if self.get_current_tab_slots_count() <= 0:
            return

        for slot in self.slots[self.currentIndex()]:
            if slot.hasFocus():
                self._layout.removeWidget(slot)
                slot.close()
                self.slots[self.currentIndex()].remove(slot)
                self.regenerate_slot_widgets_layout()
                break

    def pop_slot(self):
        if self.count() <= 0:
            return

        slots_count = self.get_current_tab_slots_count()
        if slots_count <= 0:
            return

        self._layout.removeWidget(self.slots[self.currentIndex()][slots_count - 1])
        self.slots[self.currentIndex()][slots_count - 1].close()
        self.slots[self.currentIndex()].pop()

        self._layout.update()

    def regenerate_slot_widgets_layout(self):
        if self.count() <= 0:
            return

        slots_count = self.get_current_tab_slots_count()
        if slots_count <= 0:
            return

        current_slots = self.slots[self.currentIndex()]

        self._layout.children().clear()

        for i in range(slots_count):
            row_index = i % 4
            if row_index < 0:
                row_index = 0
            column_index = floor(i / 4)
            if column_index < 0:
                column_index = 0

            self._layout.addWidget(current_slots[i], row_index, column_index, 1, 1)

        self._layout.update()
