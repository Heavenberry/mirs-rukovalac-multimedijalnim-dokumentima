from plugin_framework.extension import Extension
from .widget import Workspace

class Plugin(Extension):
    def __init__(self, specification, iface):
        """
        :param iface: main_window aplikacije
        """
        super().__init__(specification, iface)
        self.widget : Workspace = Workspace(iface)
        self.deactivate()

    # FIXME: implementacija apstraktnih metoda
    def activate(self):
        self.activated = True
        self.widget.setHidden(False)
        print("Activated Workspace Plugin")

    def deactivate(self):
        self.activated = False
        self.widget.setHidden(True)
        print("Deactivated Workspace Plugin")