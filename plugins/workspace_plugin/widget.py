from PySide2 import QtWidgets
from PySide2.QtWidgets import *
from PySide2.QtCore import *

class Workspace(QtWidgets.QDockWidget):
    widget_for = 1
    treeWidget = QTreeWidget()
    def __init__(self, parent):
        super().__init__(parent)
        self._layout = QtWidgets.QVBoxLayout()
        self.setObjectName(u"workSpaceWidget")
        self.setMinimumSize(QSize(250, 250))
        self.setWindowTitle("Workspace Plugin")

        widget_contents = QWidget(self)
        widget_contents.setObjectName(u"dockWidgetContents")
        grid_layout = QGridLayout(widget_contents)

        grid_layout.setObjectName(u"grid_layout")
        self.treeWidget = QTreeWidget(widget_contents)
        self.treeWidget.setObjectName(u"tree_widget")
        self.treeWidget.setGeometry(QRect(0, 20, 250, 250))
        self.treeWidget.setMinimumSize(QSize(250, 250))
        self.treeWidget.setHeaderLabel("")
        self.treeWidget.itemClicked.connect(self.open_document)

        tool_bar = QtWidgets.QToolBar("workspace_toolbar", self)

        tool_bar.addAction(parent.actions_dict["new_workspace"])
        tool_bar.addAction(parent.actions_dict["delete_workspace"])
        tool_bar.addAction(parent.actions_dict["new_collection"])
        tool_bar.addAction(parent.actions_dict["delete_collection"])
        tool_bar.addAction(parent.actions_dict["new_document"])
        tool_bar.addAction(parent.actions_dict["delete_document"])

        parent.actions_dict["new_workspace"].triggered.connect(self.create_new_workspace)
        parent.actions_dict["delete_workspace"].triggered.connect(self.delete_workspace)
        parent.actions_dict["new_collection"].triggered.connect(self.create_new_collection)
        parent.actions_dict["delete_collection"].triggered.connect(self.delete_collection)
        parent.actions_dict["new_document"].triggered.connect(self.create_new_document)
        parent.actions_dict["delete_document"].triggered.connect(self.delete_document)

        grid_layout.setMenuBar(tool_bar)
        grid_layout.addWidget(self.treeWidget, 1, 0, 1, 1)
        self.setWidget(widget_contents)
        parent.addDockWidget(Qt.LeftDockWidgetArea, self)

    def create_new_workspace(self):
        new_workspace_item = QTreeWidgetItem(self.treeWidget)
        new_workspace_item.setText(0,"New Workspace..")
        new_workspace_item.setFlags(Qt.ItemIsEditable | Qt.ItemIsSelectable | Qt.ItemIsEnabled)
        new_workspace_item.setWhatsThis(0, "workspace")
        new_workspace_item.setExpanded(True)
        self.treeWidget.addTopLevelItem(new_workspace_item)
        self.treeWidget.editItem(new_workspace_item, 0)

    def create_new_collection(self):
        selected_items = self.treeWidget.selectedItems()
        if len(selected_items) != 1:
            return

        current_item = self.treeWidget.currentItem()
        if current_item.whatsThis(0) != "workspace":
            return

        new_collection_item = QTreeWidgetItem(current_item)
        new_collection_item.setText(0,"New Collection...")
        new_collection_item.setWhatsThis(0, "collection")

        new_collection_item.setFlags(Qt.ItemIsEditable | Qt.ItemIsSelectable | Qt.ItemIsEnabled)

        current_item.addChild(new_collection_item)
        current_item.setExpanded(True)
        self.treeWidget.editItem(new_collection_item, 0)

    def create_new_document(self):
        selected_items = self.treeWidget.selectedItems()
        if len(selected_items) != 1:
            return

        current_item = self.treeWidget.currentItem()
        if current_item.whatsThis(0) != "collection":
            return

        new_document_item = QTreeWidgetItem(current_item)
        new_document_item.setText(0, "New Document...")
        new_document_item.setWhatsThis(0, "document")

        new_document_item.setFlags(Qt.ItemIsEditable | Qt.ItemIsSelectable | Qt.ItemIsEnabled)

        current_item.addChild(new_document_item)
        current_item.setExpanded(True)
        
        self.treeWidget.editItem(new_document_item, 0)

    def open_document(self, it : QTableWidgetItem, col):
        if it.whatsThis(0) != "document":
            return

        for plugin in self.parent().get_plugin_registry().get_plugins():
            if plugin.plugin_specification.id == 2:
                tab = QWidget()
                tab.setObjectName(it.text(0))

                duplicate_found = False
                duplicate_tab_index = -1
                for i in range(0,plugin.widget.count()):
                    child_widget = plugin.widget.widget(i)
                    if child_widget.objectName() == tab.objectName():
                        duplicate_found = True
                        duplicate_tab_index = i
                        break

                if duplicate_found and duplicate_tab_index != -1:
                    plugin.widget.remove_all_slots_from_tab_index(duplicate_tab_index)
                    plugin.widget.removeTab(duplicate_tab_index)

                plugin.widget.insertTab(plugin.widget.count(), tab, it.text(0))
                break

    def delete_workspace(self):
        selected_items = self.treeWidget.selectedItems()
        if len(selected_items) != 1:
            return

        current_item = self.treeWidget.currentItem()
        if current_item.whatsThis(0) != "workspace":
            return

        self.treeWidget.takeTopLevelItem(self.treeWidget.indexOfTopLevelItem(current_item))

    def delete_collection(self):
        selected_items = self.treeWidget.selectedItems()
        if len(selected_items) != 1:
            return

        current_item = self.treeWidget.currentItem()
        if current_item.whatsThis(0) != "collection":
            return

        current_item.parent().removeChild(current_item)

    def delete_document(self):
        selected_items = self.treeWidget.selectedItems()
        if len(selected_items) != 1:
            return

        current_item = self.treeWidget.currentItem()
        if current_item.whatsThis(0) != "document":
            return

        current_item.parent().removeChild(current_item)
