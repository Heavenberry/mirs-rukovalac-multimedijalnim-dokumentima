from PySide2 import QtWidgets


class DocWidget(QtWidgets.QDialog):
    # FIXME: postaviti relativnu putanju
    config_path = "configuration.json"
    def __init__(self, parent=None):
        super().__init__(parent)
        self._layout = QtWidgets.QVBoxLayout()
        self._integrativna_label = QtWidgets.QLabel("Integrativna:")
        self._workspace_label = QtWidgets.QLabel("Workspace:")
        self._dokument_handler_label = QtWidgets.QLabel("Dokument:")
        self.open_dokument_handler_label = QtWidgets.QLabel("Otvoren dokument:")
        self.page_handler_label = QtWidgets.QLabel("Tekuća stranica:")
        self.storage_label = QtWidgets.QLabel("Skladištenje:")
        self.administration_label = QtWidgets.QLabel("Administracija:")

        self._populate_layout()
        self.setLayout(self._layout)
        self.setWindowTitle("Dokumentacija o komponentama")
        self.resize(300, 128)


    def _populate_layout(self):
        # FIXME: procitati podatke iz konfiguracije i prepisati stringove (labele)
        self._layout.addWidget(self._integrativna_label)
        self._layout.addWidget(QtWidgets.QLabel("""    Integrativna komponenta predstavlja centralnu komponentu koja povezuje ostale komponente i komunicira sa njima.
    Preko nje se aktiviraju i instaliraju ostale obavezne komponente.
    Mogu se aktivirati/instalirati pritiskom na taster plugin manager koji se nalazi na toolbar-u ili u file-menu opcijama."""))
        self._layout.addWidget(self._workspace_label)
        self._layout.addWidget(QtWidgets.QLabel("""    Workspace je obavezna komponenta koja prikazuje radna okruženja, kolekcije i dokumente koji sadrže tipove fajlova nad kojima radimo.
    Sa workspace komponentom možemo kreirati/obrisati nova radna okruženja, kolekcije i dokumente.
    Takođe je moguće prebacivati dokumente u različite kolekcije ili radne prostore.
    Može se aktivirati/deaktivirati preko plugin manager-a pritiskom na toolbar-u ili odlaskom na file meni."""))
        self._layout.addWidget(self._dokument_handler_label)
        self._layout.addWidget(QtWidgets.QLabel("""    Dokumente možemo praviti/brisati, otvarati/zatvarati, preimenovati i prebacivati u druge kolekcije."""))
        self._layout.addWidget(self.open_dokument_handler_label)
        self._layout.addWidget(QtWidgets.QLabel("""    Kroz otvoreni dokument možemo da ga pretražujemo i zatvorimo. 
    Otvorenom dokumentu možemo da kreiramo/brišemo stranice, da ih pomeramo i delimo."""))
        self._layout.addWidget(self.page_handler_label)
        self._layout.addWidget(QtWidgets.QLabel("""    Uređujemo tekuću stranicu praveći/brisajući slotove. Slotove možemo da pomeramo i menjamo dimenzije. 
    Različiti monotipovi (mp3, bmp, txt, mp4, jpeg, png) se otvaraju unutar slota nad kojima se može raditi."""))
        self._layout.addWidget(self.storage_label)
        self._layout.addWidget(QtWidgets.QLabel("""    Rad nad monotipovima, slotovima, stranicama, dokumentima kao i izmene u workspace-u i podaci o korisnicima će biti skladišteni."""))
        self._layout.addWidget(self.administration_label)
        self._layout.addWidget(QtWidgets.QLabel("""    Administraciona komponenta ima uvid u broj korisnika, daje i uklanja pristup korisnicima i delegira nadležnosti."""))
