from PySide2 import QtWidgets
from plugin_framework.extension import Extension
from .widgets.info_widget import InfoWidget
from .widgets.documentation_widget import DocWidget

class Plugin(Extension):
    def __init__(self, specification, iface):
        """
        :param iface: main_window aplikacije
        """
        super().__init__(specification, iface)
        # TODO: ukoliko u nekom plugin-u treba sacuvati referencu na iface, napraviti atribut
        self.widget = InfoWidget(iface)
        self.open_action = QtWidgets.QAction("&About")
        self.open_doc = QtWidgets.QAction("&Documentation")
        self.open_action.triggered.connect(self.open_help)
        self.open_doc.triggered.connect(self.open_documentation)
        print("Help plugin initialized!")
        self.deactivate()

    # FIXME: implementacija apstraktnih metoda
    def activate(self):
        self.iface.add_menu_action("&Help", self.open_action)
        self.iface.add_menu_action("&Help", self.open_doc)
        self.activated = True
        print("Activated Help Plugin")

    def deactivate(self):
        self.iface.remove_menu_action("&Help", self.open_action)
        self.iface.remove_menu_action("&Help", self.open_doc)
        self.activated = False
        self.widget.setHidden(True)
        print("Deactivated Help Plugin")

    def open_help(self):
        self.widget.show()

    def open_documentation(self):
        widget = DocWidget(self.iface)
        widget.show()
        