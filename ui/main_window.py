from PySide2 import QtWidgets, QtGui
from .plugin_manager import PluginManager
from PySide2.QtWidgets import *
from datetime import date
import webbrowser

class MainWindow(QtWidgets.QMainWindow):
    # TODO: load from config
    listOfOpenFiles = list()

    def __init__(self, config, parent=None):
        super().__init__(parent)
        self.config = config

        self.setWindowTitle(self.config["title"])
        self.setWindowIcon(QtGui.QIcon(self.config["icon"]))
        self.resize(self.config["window_size"]["width"], self.config["window_size"]["height"])

        self.listOfActivatedPluginIds = list(self.config["activated_plugins"])

        # registar plugin-ova
        self.plugin_registry = None
        #meni
        self.menu_bar = QtWidgets.QMenuBar(self)
        #toolbar
        self.tool_bar = QtWidgets.QToolBar("Toolbar",self)
        #statusbar
        self.status_bar = QtWidgets.QStatusBar(self)
        self.status_bar.show()
        #centralwidget
        self.central_widget = QtWidgets.QWidget(self)

        self.centralWidgetGridLayout = QGridLayout(self.central_widget)
        self.centralWidgetGridLayout.setObjectName(u"centralWidgetGridLayout")

        self.actions_dict = {
            
            "quit": QtWidgets.QAction(QtGui.QIcon("resources/icons/application--minus.png"), "&Quit", self),
            "plugin_manager": QtWidgets.QAction(QtGui.QIcon("resources/icons/brain--plus.png"), "&Plugin Manager", self),
            "new_workspace": QtWidgets.QAction(QtGui.QIcon("resources/icons/book--plus.png"), "New &Workspace",self),
            "delete_workspace": QtWidgets.QAction(QtGui.QIcon("resources/icons/book--minus.png"), "Delete Wor&kspace",self),
            "new_collection": QtWidgets.QAction(QtGui.QIcon("resources/icons/blue-folder--plus.png"), "New &Collection",self),
            "delete_collection": QtWidgets.QAction(QtGui.QIcon("resources/icons/blue-folder--minus.png"), "Delete Co&llection",self),
            "new_document": QtWidgets.QAction(QtGui.QIcon("resources/icons/blue-document--plus.png"), "New &Document",self),
            "delete_document": QtWidgets.QAction(QtGui.QIcon("resources/icons/blue-document--minus.png"), "Delete Doc&ument",self),
            "save": QtWidgets.QAction(QtGui.QIcon("resources/icons/box-document.png"), "&Save File",self),
            "load": QtWidgets.QAction(QtGui.QIcon("resources/icons/briefcase--plus.png"), "&Load File",self),
            "undo": QtWidgets.QAction(QtGui.QIcon("resources/icons/arrow-curve-180.png"), "&Undo", self),
            "cut": QtWidgets.QAction(QtGui.QIcon("resources/icons/bookmark--exclamation.png"), "&Cut", self),
            "copy": QtWidgets.QAction(QtGui.QIcon("resources/icons/books.png"),"&Copy",self ),
            "paste": QtWidgets.QAction(QtGui.QIcon("resources/icons/block.png"), "&Paste", self),
            "new_slot": QtWidgets.QAction(QtGui.QIcon("resources/icons/application--plus.png"), "&New Slot", self),
            "delete_slot": QtWidgets.QAction(QtGui.QIcon("resources/icons/application--minus.png"), "&Delete Slot", self),
            "pop_slot": QtWidgets.QAction(QtGui.QIcon("resources/icons/application--arrow.png"), "&Pop Slot", self),
            "open_guide":QtWidgets.QAction("&Guide", self)
        }

        self._bind_actions()

        self._populate_menu_bar()
        self._populate_tool_bar()

        self.setMenuBar(self.menu_bar)
        self.addToolBar(self.tool_bar)
        self.setStatusBar(self.status_bar)
        self.setCentralWidget(self.central_widget)

        self.set_status_message("Datum: " + date.today().strftime("%d/%m/%Y"))

    def _populate_tool_bar(self):
        self.tool_bar.addAction(self.actions_dict["plugin_manager"])
        self.tool_bar.addAction(self.actions_dict["quit"])

    def _populate_menu_bar(self):
        file_menu = QtWidgets.QMenu("&File", self.menu_bar)
        plugins_menu = QtWidgets.QMenu("&Plugins", self.menu_bar)
        credentials_menu = QtWidgets.QMenu("&Credentials", self.menu_bar)
        help_menu = QtWidgets.QMenu("&Help", self.menu_bar)

        file_menu.addAction(self.actions_dict["quit"])
        help_menu.addAction(self.actions_dict["open_guide"])

        plugins_menu.addAction(self.actions_dict["plugin_manager"])

        self.menu_bar.addMenu(file_menu)
        self.menu_bar.addMenu(plugins_menu)
        self.menu_bar.addMenu(credentials_menu)
        self.menu_bar.addMenu(help_menu)


    def _bind_actions(self):
        self.actions_dict["plugin_manager"].triggered.connect(self.open_plugin_manager)
        self.actions_dict["quit"].setShortcut("Ctrl+Q")
        self.actions_dict["quit"].triggered.connect(self.close)
        self.actions_dict["open_guide"].triggered.connect(self.open_guide)

    def add_plugin_registry(self, registry):
        self.plugin_registry = registry

    def get_plugin_registry(self):
        return self.plugin_registry

    # ********************************** #
    # Metode koje sluze za obradu akcija #
    # ********************************** #
    def open_plugin_manager(self):
        manager = PluginManager(self, self.plugin_registry)
        manager.show()
    
    def open_guide(self):
        htmlfile = "html\dokumentacija.html"
        webbrowser.open(htmlfile)

    # *************************************** #
    # Metode koje ce koristiti drugi widget-i #
    # *************************************** #
    def set_status_message(self, message=""):
        self.status_bar.clearMessage()
        self.status_bar.showMessage(message)

    def add_menu_action(self, menu_name, action):
        menues = self.menu_bar.findChildren(QtWidgets.QMenu)
        for menu in menues:
            if menu.title() == menu_name:
                menu.addAction(action)
                break
    
    def remove_menu_action(self, menu_name, action):
        menues = self.menu_bar.findChildren(QtWidgets.QMenu)
        for menu in menues:
            if menu.title() == menu_name:
                menu.removeAction(action)
                break

    def add_menu(self, menu):
        self.menu_bar.addMenu(menu)

    def activate_plugins_from_conf(self):
        if self.listOfActivatedPluginIds == None or self.plugin_registry == None:
            return

        for pluginId in self.listOfActivatedPluginIds:
            for plugin in self.plugin_registry._plugins:
                if plugin.plugin_specification.id == int(pluginId):
                    plugin.activate()

